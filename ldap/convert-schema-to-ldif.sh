#!/bin/sh

SLAPD_CONF=`mktemp --suffix=fca`
SLAPD_DIR=`mktemp -d --suffix=fca`

trap "rm -rf $SLAPD_DIR $SLAPD_CONF" EXIT

cat >$SLAPD_CONF <<EOF
include /etc/ldap/schema/core.schema
include `pwd`/schema/franceconnect.schema
EOF

/usr/sbin/slaptest -f $SLAPD_CONF -F $SLAPD_DIR

cp $SLAPD_DIR/cn\=config/cn\=schema/cn\=\{1\}franceconnect.ldif schema/franceconnect.ldif

